package com.android.systemui.qs.tiles;
 
import android.content.res.Configuration;
import android.content.res.Resources;
 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
 
import com.android.systemui.R;
//import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.tileimpl.QSTileImpl;
import android.content.Intent;
import android.util.Log;
 
import android.content.Context;
import com.android.internal.logging.MetricsLogger;
//import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.logging.nano.MetricsProto.MetricsEvent;
 
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.Message;
import android.os.Messenger;
import android.os.Handler;
import android.os.IBinder;
import android.os.UserHandle;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.systemui.plugins.qs.QSTile.BooleanState;
import com.android.systemui.qs.QSHost;
import android.widget.Switch;
 

public class ScreenShotTile extends QSTileImpl<BooleanState> {
 
    public ScreenShotTile(QSHost host) {
        super(host);
    }
 
    @Override
    public BooleanState newTileState() {
        return new BooleanState();
    }
 
    @Override
    protected void handleClick() {
        mHost.screenShot();
    }
 
    @Override
    public int getMetricsCategory() {
        return MetricsEvent.QS_PANEL;
    }
 
    @Override
    public Intent getLongClickIntent() {
        return new Intent(mContext,com.android.systemui.screenshot.ScreenShotActivity.class);
    }
 
    @Override
    public CharSequence getTileLabel() {
        return mContext.getString(R.string.quick_settings_screenshot);
    }
 
    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        state.label = mContext.getString(R.string.quick_settings_screenshot);
        state.icon = state.icon = ResourceIcon.get(R.drawable.ic_qs_screenshot);
        state.value = false;
        //return;
        state.expandedAccessibilityClassName = Switch.class.getName();
    }
 
    public void setListening(boolean listening) {
    }
 

     @Override
    public void handleSetListening(boolean listening) {
        // if (mController == null) return;
        // if (DEBUG) Log.d(TAG, "handleSetListening " + listening);
        // if (listening) {
        //     mController.addCallback(mCallback);
        //     mKeyguard.addCallback(mCallback);
        // } else {
        //     mController.setDiscovering(false);
        //     mController.removeCallback(mCallback);
        //     mKeyguard.removeCallback(mCallback);
        // }
    }
    final Object mScreenshotLock = new Object();
    ServiceConnection mScreenshotConnection = null;
    private Handler mHandler = new Handler();
 
    final Runnable mScreenshotTimeout = new Runnable() {
        @Override public void run() {
            synchronized (mScreenshotLock) {
                if (mScreenshotConnection != null) {
                    mContext.unbindService(mScreenshotConnection);
                    mScreenshotConnection = null;
                }
            }
        }
    };
 
    private final Runnable mScreenshotRunnable = new Runnable() {
        @Override
        public void run() {
            takeScreenshot();
        }
    };
 
    private void takeScreenshot() {
        synchronized (mScreenshotLock) {
            if (mScreenshotConnection != null) {
                return;
            }
            ComponentName cn = new ComponentName("com.android.systemui",
                    "com.android.systemui.screenshot.TakeScreenshotService");
            Intent intent = new Intent();
            intent.setComponent(cn);
            ServiceConnection conn = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    synchronized (mScreenshotLock) {
                        if (mScreenshotConnection != this) {
                            return;
                        }
                        Messenger messenger = new Messenger(service);
                        Message msg = Message.obtain(null, 1);
                        final ServiceConnection myConn = this;
                        Handler h = new Handler(mHandler.getLooper()) {
                            @Override
                            public void handleMessage(Message msg) {
                                synchronized (mScreenshotLock) {
                                    if (mScreenshotConnection == myConn) {
                                        mContext.unbindService(mScreenshotConnection);
                                        mScreenshotConnection = null;
                                        mHandler.removeCallbacks(mScreenshotTimeout);
                                    }
                                }
                            }
                        };
                        msg.replyTo = new Messenger(h);
                        msg.arg1 = msg.arg2 = 0;
                        try {
                            messenger.send(msg);
                        } catch (RemoteException e) {
                        }
                    }
                }
                @Override
                public void onServiceDisconnected(ComponentName name) {}
            };
            if (mContext.bindServiceAsUser(
                    intent, conn, Context.BIND_AUTO_CREATE, UserHandle.CURRENT)) {
                mScreenshotConnection = conn;
                mHandler.postDelayed(mScreenshotTimeout, 15000);
            }
        }
    }
}
